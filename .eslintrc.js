module.exports = {
  env: {
    "browser": true,
    "node": true,
  },
  extends: [
    "airbnb-typescript/base",
    "eslint:recommended",
    "plugin:@typescript-eslint/recommended",
    "plugin:@typescript-eslint/recommended-requiring-type-checking",
  ],
  rules: {
    "no-console": "off",
    "@typescript-eslint/no-namespace": "off",
    'arrow-body-style': ['error', 'always'],
    "@typescript-eslint/lines-between-class-members": ["error", "always", { exceptAfterSingleLine: true }],
    "@typescript-eslint/no-namespace": "off",
  },
  parserOptions: {
    project: "./tsconfig.json",
  },
  ignorePatterns: [
    ".eslintrc.js",
    "*.js"
  ],
};