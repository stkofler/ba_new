# How to run

## Install dependencies

```bash
npm i
```

## Usage

```bash
npm run dev
or
npm run dev -- 8081
```

Then follow instructions on <http://localhost:8081>.

No need for .env as the database connection is hardcoded.

## Manual Testing

The UI is there for manual testing and allows for:

    1. uploading a csv file
    2. downloading pdf file by inserting any email from the csv

Two csv files can be found in folder **manual-testing/**

PDFs will be created in **resources/licences/pdf/**

Uploaded CSVs are currently not cleaned up from **resources/uploads/csv/** but ​rerunning the csv upload just updates PDFs and DB entries.

The console should output the result of uploading. Whatever succeeds there will be in MongoDB cloud database. Technically the result could be used for the nightly stat report.