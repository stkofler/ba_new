export default {
  DB_CONNECT: process.env.DB_CONNECT ?? 'mongodb+srv://user_234:user_345@cluster0.v8wbz.mongodb.net/BA?retryWrites=true&w=majority',
  PORT_FRONTEND: process.env.PORT_FRONTEND ?? 8080,
  PORT_BACKEND: process.env.PORT_BACKEND ?? process.argv[2] ?? 8081,
};
