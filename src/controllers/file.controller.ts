import { Request, Response } from 'express';
import config from '../config';
import CsvParser from '../core/licence/CsvParser';
import UserService from '../database/services/user.service';

type FileDownloadQuery = {
  email: string
};

class FileController {
  constructor(private dbService: UserService) {}

  /**
   * would just signal upload outcome but currently for sake of testing starts the csv parser
   */
  uploadCsv = async (req: Request, res: Response): Promise<void> => {
    if (req.file === undefined) {
      res.status(400).send('Please upload a file!');
    } else {
      // This part would be run from a scheduler and not here.
      const csvParser = new CsvParser(UserService.getInstance());
      await csvParser.start();
      res.setHeader('Content-Type', 'text/html');
      res.status(200).send(`<p>Uploaded the file successfully!<br>
      It can be downloaded from <a href="http://localhost:${config.PORT_BACKEND}/download">here</a></p>`);
    }
  };

  /**
   * gets email from req, gets user by email from db and starts licence download
   */
  downloadPdf = async (req: Request, res: Response): Promise<void> => {
    const { email } = req.query as FileDownloadQuery;
    const existingUser = await this.dbService.getByEmail(email);
    if (existingUser) {
      if (existingUser.licence.path) {
        res.status(200).download(existingUser.licence.path, 'licence.pdf');
      } else {
        res.status(404).send('No Licence found');
      }
    } else {
      res.status(404).send('User not found');
    }
  };
}

export default FileController;
