import fs from 'fs';
import neatCsv from 'neat-csv';
import path from 'path';
import User from '../../database/models/user.model';
import UserService from '../../database/services/user.service';
import PdfHandler from './PdfHandler';

interface CsvRow {
  name: string,
  email: string,
}

class CsvParser {
  private static PATH_CSV = path.join(__dirname, '../../../resources/uploads/csv');
  private static PATH_PDF = path.join(__dirname, '../../../resources/licences/pdf');

  constructor(private userService: UserService) { }

  /**
   * iterates each file in resources/uploads/csv
   *    converts to array of type CsvRows
   *    stores and prints result of storing those CsvRows as Licences
   *    technically this result could be used for the nightly stats email
   */
  start = async (): Promise<void> => {
    const fileNames: string[] = await CsvParser.getFileList();
    fileNames
      .filter((fileName) => { return /\.csv$/.exec(path.extname(fileName).toLowerCase()); })
      .map(async (fileName) => {
        const csvRows: CsvRow[] = await CsvParser.getCsvRows(fileName);
        const result = await this.storeLicences(csvRows);
        console.log(result);
      });
  };

  /**
   * actually parse the filecontents and convert each row to array of CsvRows
   */
  private static getCsvRows = async (fileName: string): Promise<CsvRow[]> => {
    const fileContent: string = await CsvParser.getFileContent(fileName);
    return neatCsv(fileContent);
  };

  /**
   * iterates through each row from the csv
   * prepares path to store the pdf and then stores
   * storing happens in two steps, one in database and one as PDF
   * should do all asynchronously and return an array of results
   */
  private storeLicences = async (csvRows: CsvRow[])
  : Promise<PromiseSettledResult<string>[]> => {
    const endDate = User.getDefaultDate();

    const result = Promise.allSettled(csvRows.map(async (csvRow) => {
      const { name, email } = csvRow;
      const pdfFileName = `${email}-${endDate.toISOString().substring(0, 10)}.pdf`;
      const fullPdfPath = `${CsvParser.PATH_PDF}\\${pdfFileName}`;

      try {
        await this.storeLicenceInDb(name, email, endDate, fullPdfPath);
        await this.storeLicenceAsPdf(name, endDate, fullPdfPath);
        return await Promise.resolve(`${email} stored correctly!`);
      } catch (e) {
        return Promise.reject((e as Error).message);
      }
    }));

    return result;
  };

  /**
   * prepares a Licence object and checks if user already exists
   * if he exists: updates his Licence entry in the database
   * if not: creates new User with new Licence
   */
  private storeLicenceInDb = async (name: string, email: string, endDate: Date, fullPdfPath: string)
  : Promise<User.UserDoc> => {
    const licence: User.Licence = {
      path: fullPdfPath,
      expires: endDate,
    };

    const userDocument = await this.userService.getByEmail(email);
    if (!userDocument) {
      const pw = CsvParser.getDefaultPassword(name);
      const user: User = new User(name, email, pw, [User.Role.CYCLIST], licence);
      return this.userService.create(user);
    }
    return this.userService.updateById(userDocument.id, { licence });
  };

  /**
   * simply stores licence as pdf
   */
  private storeLicenceAsPdf = async (name: string, endDate: Date, fullPdfPath: string)
  : Promise<void> => {
    const pdfText = `Licence for cyclist: ${name}!\nExpires: ${endDate.toDateString()}`;
    return PdfHandler.savePdfToFile(pdfText, fullPdfPath);
  };

  private static getFileList = async (): Promise<string[]> => {
    try {
      return await fs.promises.readdir(CsvParser.PATH_CSV);
    } catch (e) {
      console.log('getFileList error:', e);
      return [];
    }
  };

  private static getFileContent = async (fileName: string): Promise<string> => {
    try {
      return await fs.promises.readFile(`${CsvParser.PATH_CSV}\\${fileName}`, 'utf-8');
    } catch (e) {
      console.log('getFileContent error:', e);
      return '';
    }
  };

  private static getDefaultPassword = (licenceName: string): string => {
    const currentYear = new Date().getFullYear();
    return `${licenceName}_${currentYear}`;
  };
}

export default CsvParser;
