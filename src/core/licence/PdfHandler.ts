import fs from 'fs';
import PDFDocument from 'pdfkit';

class PdfHandler {
  /**
   * creates file stream and a pdf package writes to it asynchronously, thangs to mentioned source
   */
  public static savePdfToFile = async (text: string, fullPdfPath: string): Promise<void> => {
    return new Promise<void>((resolve) => {
      // source: https://github.com/foliojs/pdfkit/issues/265#issuecomment-246564718

      let pendingStepCount = 2;
      const stepFinished = () => {
        pendingStepCount -= 1;
        if (pendingStepCount === 0) { resolve(); }
      };
      const doc = new PDFDocument();

      const writeStream = fs.createWriteStream(fullPdfPath);
      writeStream.on('close', stepFinished);
      doc.pipe(writeStream);

      doc.fontSize(25).text(text, 100, 100);

      doc.end();
      stepFinished();
    });
  };
}
export default PdfHandler;
