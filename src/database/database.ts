import mongoose from 'mongoose';

class DbHandler {
  connect = async (dbconfig: string): Promise<typeof mongoose> => {
    return mongoose.connect(dbconfig, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
      useCreateIndex: true,
    });
  };
}

export default DbHandler;
