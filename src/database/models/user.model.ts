import { Document, model, Schema } from 'mongoose';

class User implements User.IUser {
  constructor(
    public name: string,
    public email: string,
    public password: string,
    public roles: User.Role[] = [User.Role.USER],
    public licence: User.Licence = {
      path: '',
      expires: User.getDefaultDate(),
    },
  ) { }

  // returns date 10 years from now
  public static getDefaultDate = (): Date => {
    const date = new Date();
    date.setFullYear(date.getFullYear() + 10);
    return date;
  };
}

namespace User {
  export enum Role {
    USER = 'USER',
    ADMIN = 'ADMIN',
    CERTIFIER = 'CERTIFIER',
    CYCLIST = 'CYCLIST',
  }
  export interface Licence {
    path: string,
    expires?: Date;
  }
  export interface IUser {
    name: string,
    email: string,
    password: string,
    roles: Role[],
    licence: Licence
  }
  export type UserDoc = IUser & Document;
}
export default User;

const userSchema: Schema = new Schema({
  name: { type: String, required: true, maxLength: 20 },
  email: { type: String, required: true, unique: true },
  password: { type: String, required: true },
  licence: {
    path: { type: String, required: true },
    expires: { type: Date, required: true },
  },
  roles: {
    type: [{
      type: String,
      enum: User.Role,
    }],
    default: User.Role.USER,
  },
}, {
  timestamps: true,
});

export const UserModel = model<User.UserDoc>('User', userSchema);
