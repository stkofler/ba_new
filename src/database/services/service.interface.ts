import { Document } from 'mongoose';

interface CRUD<T extends Document> {
  create: (resource: T) => Promise<T>,
  getById: (id: string) => Promise<T | null>,
  getList: (limit: number) => Promise<T[]>,
  deleteById: (id: string) => Promise<void>,
  updateById: (id: string, partialResource: Partial<T>) => Promise<T>,
}

export default CRUD;
