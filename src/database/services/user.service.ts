import User, { UserModel } from '../models/user.model';
import CRUD from './service.interface';

class UserService implements CRUD<User.UserDoc> {
  private static instance: UserService;

  // eslint-disable-next-line @typescript-eslint/no-empty-function
  private constructor() {}

  public static getInstance(): UserService {
    if (!UserService.instance) {
      UserService.instance = new UserService();
    }
    return UserService.instance;
  }

  create = async (user: User.IUser): Promise<User.UserDoc> => {
    const newUser = new UserModel(user);
    return newUser.save();
  };

  getById = async (id: string): Promise<User.UserDoc | null> => {
    return UserModel.findById({ _id: id }).exec();
  };

  getList = async (limit = 5): Promise<User.UserDoc[]> => {
    return UserModel.find().limit(limit).exec();
  };

  deleteById = async (id: string): Promise<void> => {
    await UserModel.findByIdAndRemove(id).exec();
  };

  updateById = async (id: string, partialUser: Partial<User.UserDoc>): Promise<User.UserDoc> => {
    const updatedUser = await UserModel.findOneAndUpdate(
      { _id: id },
      { $set: partialUser },
      { useFindAndModify: false },
    ).exec();

    if (!updatedUser) {
      throw new Error(`User with id: ${id} not found`);
    }
    return updatedUser;
  };

  getByEmail = async (email: string): Promise<User.UserDoc | null> => {
    return UserModel.findOne({ email }).exec();
  };
}

export default UserService;
