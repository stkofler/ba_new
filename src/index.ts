import config from './config';
import DbHandler from './database/database';
import router from './routes/router';

const startServer = async () => {
  const dbHandler = new DbHandler();
  try {
    await dbHandler.connect(config.DB_CONNECT);
    console.log('Successfully connected to MongoDB.');
  } catch (e) {
    console.error('Connection: ', e, 'Exiting nodejs');
    process.exit();
  }

  const PORT = config.PORT_BACKEND;
  router.listen(PORT, () => {
    console.log(`Server is up, please go to http://localhost:${PORT}.`);
  });
};

startServer().catch((error) => { return console.log(error); });
