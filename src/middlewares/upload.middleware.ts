import { NextFunction, Request, Response } from 'express';
import multer, { FileFilterCallback } from 'multer';
import path from 'path';
import util from 'util';

type MulterFile = Express.Multer.File;

class FileUploader {
  private upload;

  private static storageOptions = multer.diskStorage({
    destination: `${__dirname}\\..\\..\\resources\\uploads\\csv\\`,
    filename: (req, file, cb) => {
      const date = new Date();
      const fullFilename = `_certifierId_${date.toISOString().substring(0, 10)}.csv`;
      cb(null, fullFilename);
    },
  });

  private static getNewFileName = (filename: string): string => {
    const d = new Date();
    return `${path.basename(filename, '.csv')}-${d.getFullYear()}-${d.getMonth()}-${d.getDay()}.csv`;
  };

  private static fileFilter = (req: Request, file: MulterFile, cb: FileFilterCallback) => {
    if (/\.csv$/.exec(file.originalname)) {
      cb(null, true);
    } else {
      req.fileValidationError = 'Only csv files are allowed!';
      cb(null, false);
    }
  };

  constructor(maxSize: number = 2 * 1024 * 1024, name = 'file') {
    this.upload = multer({
      storage: FileUploader.storageOptions,
      fileFilter: FileUploader.fileFilter,
      limits: { fileSize: maxSize },
    }).single(name);
  }

  startUpload = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    try {
      const upload = util.promisify(this.upload);
      await upload(req, res);

      const { fileValidationError } = req;
      if (!fileValidationError) {
        next();
      } else {
        res.status(500).send(fileValidationError);
      }
    } catch (e: unknown) {
      if (e instanceof multer.MulterError && e.code === 'LIMIT_FILE_SIZE') {
        res.status(500).send('File size cannot be larger than 2MB!');
      } else {
        res.status(500).send(`Could not upload the file. ${JSON.stringify(e)}`);
      }
    }
  };
}

export default FileUploader;
