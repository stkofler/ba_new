import { Router } from 'express';
import FileController from '../controllers/file.controller';
import UserService from '../database/services/user.service';
import FileUploader from '../middlewares/upload.middleware';

const fileRouter = Router();

const userService = UserService.getInstance();
const fileController = new FileController(userService);
const fileUploader = new FileUploader();

fileRouter.get('/download_pdf', [fileController.downloadPdf]);

fileRouter.post('/upload_csv', [fileUploader.startUpload], fileController.uploadCsv);
export default fileRouter;
