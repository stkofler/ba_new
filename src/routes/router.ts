import cors from 'cors';
import express from 'express';
import helmet from 'helmet';
import morgan from 'morgan';
import config from '../config';
import fileRouter from './file.routes';

const corsOptions = {
  origin: `http://localhost:${config.PORT_FRONTEND}`,
};

const app: express.Express = express();
app.use(cors(corsOptions));
app.use(helmet());
app.use(morgan('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));

// API ROUTES
app.get('/', (req, res) => {
  res.setHeader('Content-Type', 'text/html');
  res.send(`<p>Click on the "Choose File" button to upload a file:</p>
  <form action="/api/file/upload_csv" method="post" enctype="multipart/form-data">
    <input type="file" id="myFile" name="file" required>
    <input type="submit">
  </form>`);
});

app.get('/download', (req, res) => {
  res.setHeader('Content-Type', 'text/html');
  res.send(`<p>Please enter any email from the csv:</p>
  <form action="/api/file/download_pdf" method="get">
    <input type="email" id="email" name="email" value="john@john.com" required>
    <input type="submit">
  </form>`);
});

app.use('/api/file', fileRouter);

export default app;
