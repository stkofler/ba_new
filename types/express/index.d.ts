/**
 * Even though one might aruge that this specific example is not perfectly correct
 * because fileValidationError exists only in Multer Requests, I wanted to keep
 * this here to remember the mechanics of global namespace extension for cleaner type checking
 */

declare namespace Express {
  interface Request {
    fileValidationError: string,
  }
}
